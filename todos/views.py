from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    # post = get_object(Post, id=id)
    banana = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=banana)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=banana)

    context = {
        # this side is for HTML: this side is refrencing the function
        # use {{ banana.name or id or whatever you can call }} in your html to use this one
        # the form you will use to pass the form into your html
        "banana": banana,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete_name = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_name.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.list.id)
    else:
        form = TodoItemForm()

    context = {
        "task_form": form,
    }
    return render(request, "todos/create_task.html", context)


def todo_item_update(request, id):
    # post = get_object(Post, id=id)
    banana = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=banana)
        if form.is_valid():
            big_banana = form.save()
            return redirect("todo_list_detail", id=big_banana.list.id)
    else:
        form = TodoItemForm(instance=banana)

    context = {
        "banana": banana,
        "task_form": form,
    }
    return render(request, "todos/edit_task.html", context)
